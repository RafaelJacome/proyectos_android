package com.example.rafael.calculadora;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements TextWatcher {

    private EditText etNumero1;
    private EditText etNumero2;

    private TextView tvResultado;
    private TextView tvOperador;

    private Button btnSuma;
    private Button btnResta;
    private Button btnDivision;
    private Button btnMultiplicacion;
    private Button btnCalcular;
    private Button btnLimpiar;

    double numero1;
    double numero2;
    String operador = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etNumero1 = findViewById(R.id.etNumero1);
        etNumero2 = findViewById(R.id.etNumero2);

        tvResultado = findViewById(R.id.tvResultado);
        tvOperador = findViewById(R.id.tvOperador);

        btnSuma = findViewById(R.id.btnSuma);
        btnResta = findViewById(R.id.btnResta);
        btnDivision = findViewById(R.id.btnDivision);
        btnMultiplicacion = findViewById(R.id.btnMultiplicacion);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);

        etNumero1.addTextChangedListener(this);
        etNumero2.addTextChangedListener(this);
    }


    public void suma(View view) {
        tvOperador.setText("+");
        operador = "+";

    }

    public void resta(View view) {
        tvOperador.setText("-");
        operador = "-";
    }

    public void division(View view) {
        tvOperador.setText("/");
        operador = "/";
    }

    public void multiplicacion(View view) {
        tvOperador.setText("*");
        operador = "*";
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (!etNumero1.getText().toString().isEmpty() && !etNumero2.getText().toString().isEmpty()) {
            numero1 = Long.parseLong(etNumero1.getText().toString());
            numero2 = Long.parseLong(etNumero2.getText().toString());
        } else {
            if (etNumero1.getText().toString().isEmpty()) {
                numero1 = 0;
            }
            if (etNumero2.getText().toString().isEmpty()) {
                numero2 = 0;
            }
        }
    }

    public void calcular(View view) {
        switch (operador) {
            case "+":
                tvResultado.setText(String.valueOf(numero1 + numero2));
                break;

            case "-":
                tvResultado.setText(String.valueOf(numero1 - numero2));
                break;

            case "/":
                if (numero2 == 0) {
                    Toast.makeText(this, "Error, no puede dividir por CERO", Toast.LENGTH_LONG).show();
                    break;
                }
                tvResultado.setText(String.valueOf(numero1 / numero2));
                break;

            case "*":
                tvResultado.setText(String.valueOf(numero1 * numero2));
                break;

            default:
                Toast.makeText(this, "Seleccione una OPERACION", Toast.LENGTH_LONG).show();
                break;

        }
    }

    public void limpiar(View view) {
        etNumero1.setText("");
        etNumero2.setText("");
        tvOperador.setText("");
        tvResultado.setText("0");
        operador = "";
    }
}
