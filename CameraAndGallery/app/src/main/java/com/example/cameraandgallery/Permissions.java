package com.example.cameraandgallery;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

import static com.example.cameraandgallery.Constants.GALLERY;

public class Permissions {

    public static boolean isGrantedPermissions(Context context, String permissionType) {
        int permission = ActivityCompat.checkSelfPermission(context, permissionType);

        return permission == PackageManager.PERMISSION_GRANTED;
    }

    public static void verifyPermission(Activity activity, String[] permissionType){
        ActivityCompat.requestPermissions(activity, permissionType, 3);
    }
}
