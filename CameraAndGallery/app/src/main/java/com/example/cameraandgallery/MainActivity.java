package com.example.cameraandgallery;

import android.Manifest;
import android.content.ClipData;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.widget.LinearLayout.HORIZONTAL;
import static com.example.cameraandgallery.Constants.GALLERY;

public class MainActivity extends AppCompatActivity {

    private ImageView imageView_camera;
    private ImageView imageView_gallery;
    private RecyclerView recyclerView;
    private PhotoAdapter photoAdapter;
    private ArrayList<String> photos;
    private File photoFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView_camera = findViewById(R.id.imageView_camera);
        imageView_gallery = findViewById(R.id.imageView_gallery);
        recyclerView = findViewById(R.id.recyclerView);
        photos = new ArrayList<>();
        imageView_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showGallery();
            }
        });
        imageView_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCamera();
            }
        });
        callAdapter();
    }

    private void showCamera() {
        if (Permissions.isGrantedPermissions(this, Manifest.permission.CAMERA)) {
            openCamera();
        } else {
            String[] typePermission = {Manifest.permission.CAMERA};
            Permissions.verifyPermission(this, typePermission);
            //showGallery();
        }
    }

    private void openCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        photoFile = null;

        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            try {
                photoFile = createImageFile();
            } catch (IOException io) {
                Toast.makeText(this, "Error creando archivo\n" + io.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

        if (photoFile != null) {
            Uri photUri = FileProvider.getUriForFile(this, getPackageName(), photoFile);
            List<ResolveInfo> resolveInfoList = getPackageManager().queryIntentActivities(takePictureIntent,
                    getPackageManager().MATCH_DEFAULT_ONLY);

            for (ResolveInfo resolveInfo : resolveInfoList) {
                String packageManager = resolveInfo.activityInfo.packageName;
                grantUriPermission(packageManager, photUri,
                        Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }

            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photUri);
            startActivityForResult(takePictureIntent, 50);
        }
    }

    private File createImageFile() throws IOException {
        String imageFileName = "Imagen" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File storageDir = this.getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        if (storageDir != null && !storageDir.exists()) {
            boolean result = storageDir.mkdir();
            if (!result) {
                return null;
            }
        }

        return File.createTempFile(imageFileName, ".jpg", storageDir);
    }

    private void callAdapter() {
        photoAdapter = new PhotoAdapter();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        photoAdapter.setArrayPhotos(photos);
        recyclerView.setAdapter(photoAdapter);
    }

    private void showGallery() {
        //Este permiso debe estar relacionado en el manifests para poder solicitarlo

        if (Permissions.isGrantedPermissions(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            openGallery();
        } else {
            String[] typePermission = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
            Permissions.verifyPermission(this, typePermission);
            //showGallery();
        }
    }

    private void openGallery() {
        //Este funcionamiento solo sirve desde la api 19 para arriba
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) { //KITKAT
            startActivityForResult(intent, Build.VERSION_CODES.KITKAT);
        } else {
            //String[] type = {"image/*"};
            // intent.putExtra(Intent.EXTRA_MIME_TYPES, type);
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            //El request code puede tener cualquier valor entero
            startActivityForResult(intent, GALLERY);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Build.VERSION_CODES.KITKAT) {
            resultGalleryKitKatLess(data.getData());
        }

        if (requestCode == GALLERY) {
            resultGalleryKitkatHigher(data);
        }

        if (requestCode == 50) {
            if (photoFile != null){
                setArrayPhotos(photoFile.getPath());
            }
        }
    }

    private void resultGalleryKitkatHigher(Intent data) {
        ClipData clipData = data.getClipData();

        if (clipData != null) {
            for (int i = 0; i < clipData.getItemCount(); i++) {
                grantUriPermission(getPackageName(), clipData.getItemAt(i).getUri(),
                        Intent.FLAG_GRANT_READ_URI_PERMISSION);
                setArrayPhotos(clipData.getItemAt(i).getUri().toString());
            }
        } else {
            resultGalleryKitKatLess(data.getData());
        }
    }

    private void resultGalleryKitKatLess(Uri data) {
        if (data != null) {
            setArrayPhotos(data.toString());
        }
    }

    private void setArrayPhotos(String photo) {
        photos.add(photo);
        photoAdapter.setArrayPhotos(photos);
        photoAdapter.notifyDataSetChanged(); //Para decirle al adaptador que algo cambio y que se actualice
    }
}
