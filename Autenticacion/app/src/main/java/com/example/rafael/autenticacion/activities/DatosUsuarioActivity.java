package com.example.rafael.autenticacion.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.rafael.autenticacion.R;
import com.example.rafael.autenticacion.models.User;
import com.squareup.picasso.Picasso;

import static com.example.rafael.autenticacion.helper.Constants.USER_OBJECT;

public class
DatosUsuarioActivity extends AppCompatActivity {

    private TextView textViewName;
    private TextView textViewUserName;
    private TextView textViewEmail;
    private ImageView imageView_profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos_usuario);

        User user = (User) getIntent().getSerializableExtra(USER_OBJECT);
        setView();
        setTextView(user);
    }

    private void setView() {
        textViewName = findViewById(R.id.textViewName);
        textViewUserName = findViewById(R.id.textViewUserName);
        textViewEmail = findViewById(R.id.textViewEmail);
        imageView_profile = findViewById(R.id.imageView_profile);
    }

    private void setTextView(User user) {
        textViewName.setText(user.getName());
        textViewUserName.setText(user.getUsername());
        textViewEmail.setText(user.getEmail());
        Picasso.get().load(user.getPhoto()).into(imageView_profile);
    }

    public void salir(View view) {
        finish();
    }
}
