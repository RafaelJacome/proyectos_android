package com.example.rafael.autenticacion.services;

import com.example.rafael.autenticacion.models.User;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

interface IServices {

    @GET("user/auth")
    Call<User> login(@Query("email") String email, @Query("password") String password);

    @GET("user")
    Call<User> loginWithToken(@Header("Authorization") String token);
}
