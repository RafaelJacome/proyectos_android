package com.example.rafael.autenticacion.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.rafael.autenticacion.R;
import com.example.rafael.autenticacion.helper.CustomSharedPreferences;
import com.example.rafael.autenticacion.helper.ValidateInternet;
import com.example.rafael.autenticacion.models.User;
import com.example.rafael.autenticacion.services.Repository;

import java.io.IOException;

import static com.example.rafael.autenticacion.helper.Constants.TOKEN;
import static com.example.rafael.autenticacion.helper.Constants.USER_OBJECT;

public class MainActivity extends AppCompatActivity implements TextWatcher {

    private EditText etUsuario;
    private EditText etContrasena;
    private Button btnIngresar;
    private ValidateInternet validateInternet;
    private Repository repository;
    private CustomSharedPreferences customSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        validateInternet = new ValidateInternet(this);
        repository = new Repository();
        //customSharedPreferences = new CustomSharedPreferences(MainActivity.this);
        customSharedPreferences = new CustomSharedPreferences(this);

        setView();
        etUsuario.addTextChangedListener(this);
        etContrasena.addTextChangedListener(this);
        verifyToken();
    }

    private void verifyToken() {
        if (customSharedPreferences.getString(TOKEN) != null) {
            //logica para ir al repositorio con el método autologin
            validateInternet();
        }
    }

    private void validateInternet() {
        if (validateInternet.isConnected()) {
            createThreadtoLoginWithToken();
        }
    }

    private void createThreadtoLoginWithToken() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    User user = repository.loginWithToken(customSharedPreferences.getString(TOKEN));
                    startIntent(user);
                    finish();
                } catch (IOException e) {
                    Toast.makeText(MainActivity.this, "AutoLogin Fail", Toast.LENGTH_SHORT).show();
                }
            }
        });
        thread.start();
    }

    private void setView() {
        etUsuario = findViewById(R.id.login_etUser);
        etContrasena = findViewById(R.id.login_etPass);
        btnIngresar = findViewById(R.id.btnIngresar);
    }

    public void login(View view) {
        if (validateInternet.isConnected()) {
            createThreadtoLogin();
        } else {
            Toast.makeText(this, "No hay conexión a INTERNET", Toast.LENGTH_SHORT).show();
        }
    }

    private void createThreadtoLogin() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                loginRepository();
                finish();
            }
        });
        thread.start();
    }

    private void loginRepository() {
        try {
            User user = repository.login(etUsuario.getText().toString(), etContrasena.getText().toString());
            customSharedPreferences.addString(TOKEN, user.getToken());
            startIntent(user);
        } catch (IOException e) {
            showToast(e.getMessage());
        }
    }

    private void showToast(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        if (etUsuario.getText().toString().trim().isEmpty() ||
                etContrasena.getText().toString().trim().isEmpty()) {
            btnIngresar.setEnabled(false);
            btnIngresar.setBackgroundResource(R.color.gray);
        } else {
            btnIngresar.setEnabled(true);
            btnIngresar.setBackgroundResource(R.color.colorPrimaryDark);
        }
    }

    private void startIntent(User user) {
        Intent intent = new Intent(MainActivity.this, DatosUsuarioActivity.class);
        intent.putExtra(USER_OBJECT, user);
        startActivity(intent);
    }
}
