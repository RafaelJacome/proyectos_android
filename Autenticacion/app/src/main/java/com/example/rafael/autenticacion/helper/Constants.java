package com.example.rafael.autenticacion.helper;

public class Constants {

    public final static String USER_OBJECT = "object";
    public final static String TOKEN = "token";
    public final static String USER = "user";
}
