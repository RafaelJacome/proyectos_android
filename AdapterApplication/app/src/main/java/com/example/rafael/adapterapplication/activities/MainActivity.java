package com.example.rafael.adapterapplication.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.rafael.adapterapplication.models.Pet;
import com.example.rafael.adapterapplication.models.Product;
import com.example.rafael.adapterapplication.R;
import com.example.rafael.adapterapplication.adapters.AdapterPets;
import com.example.rafael.adapterapplication.adapters.AdapterProducts;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView products_recyclerView;
    private RecyclerView pets_recyclerView;

    private AdapterProducts adapterProducts;
    private AdapterPets adapterPets;

    LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        products_recyclerView = findViewById(R.id.products_recyclerView);
        pets_recyclerView = findViewById(R.id.pets_recyclerView);

        loadAdapter();
    }

    private void loadAdapter() {
        loadProductsAdapter();
        loadPetsAdapter();
    }

    public void loadProductsAdapter() {
        Product product = new Product();
        product.setProductName("Empanada");
        product.setProductDescription("Deliciosas empanadas a tan solo 1.000 pesitosw");

        ArrayList<Product> productArrayList = new ArrayList<>();
        productArrayList.add(product);
        productArrayList.add(product);
        productArrayList.add(product);
        adapterProducts = new AdapterProducts(productArrayList);

        linearLayoutManager = new LinearLayoutManager(this);
        products_recyclerView.setLayoutManager(linearLayoutManager);
        products_recyclerView.setAdapter(adapterProducts);
    }

    public void loadPetsAdapter() {
        Pet pet = new Pet();
        pet.setNombre("Rocky");
        pet.setRaza("ChowChow");
        pet.setEdad("6 Meses");

        ArrayList<Pet> petArrayList = new ArrayList<>();
        petArrayList.add(pet);
        petArrayList.add(pet);
        petArrayList.add(pet);
        petArrayList.add(pet);
        petArrayList.add(pet);
        adapterPets = new AdapterPets(petArrayList);

        linearLayoutManager = new LinearLayoutManager(this);
        pets_recyclerView.setLayoutManager(linearLayoutManager);
        pets_recyclerView.setAdapter(adapterPets);

    }
}
