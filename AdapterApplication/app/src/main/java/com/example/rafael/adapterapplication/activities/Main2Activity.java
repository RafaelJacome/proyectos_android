package com.example.rafael.adapterapplication.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.rafael.adapterapplication.fragments.OneFragment;
import com.example.rafael.adapterapplication.R;
import com.example.rafael.adapterapplication.fragments.TwoFragment;

public class Main2Activity extends AppCompatActivity {

    private TextView textView1;
    private TextView textView2;
    private FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        textView1 = findViewById(R.id.textView1);
        textView2 = findViewById(R.id.textView2);
        frameLayout = findViewById(R.id.frameLayout);

        showFragment(new OneFragment());
    }

    public void showFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.replace(frameLayout.getId(), fragment);
        fragmentTransaction.commit();
    }

    public void showFragment(View view) {
        Fragment fragment;

        if(textView1.getId() == view.getId()){
            fragment = new OneFragment();
        } else {
            fragment = new TwoFragment();
        }
        showFragment(fragment);
    }
}
