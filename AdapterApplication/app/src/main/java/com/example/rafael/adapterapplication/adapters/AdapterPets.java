package com.example.rafael.adapterapplication.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.rafael.adapterapplication.models.Pet;
import com.example.rafael.adapterapplication.R;

import java.util.ArrayList;

public class AdapterPets extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<Pet> petArrayList;

    public AdapterPets(ArrayList<Pet> petArrayList) {
        this.petArrayList = petArrayList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_pets, viewGroup, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        AdapterPets.CustomViewHolder customViewHolder = (AdapterPets.CustomViewHolder) viewHolder;
        Pet pet = petArrayList.get(position);
        customViewHolder.textViewName.setText(pet.getNombre());
        customViewHolder.textViewRaza.setText(pet.getRaza());
        customViewHolder.textViewEdad.setText(pet.getEdad());
    }

    @Override
    public int getItemCount() {
        return petArrayList.size();
    }

    private class CustomViewHolder extends RecyclerView.ViewHolder {
        private TextView textViewName;
        private TextView textViewRaza;
        private TextView textViewEdad;

        public CustomViewHolder(View view) {
            super(view);
            textViewName = itemView.findViewById(R.id.textViewName);
            textViewRaza = itemView.findViewById(R.id.textViewRaza);
            textViewEdad = itemView.findViewById(R.id.textViewEdad);
        }
    }
}
