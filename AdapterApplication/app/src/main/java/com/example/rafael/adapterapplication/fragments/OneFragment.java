package com.example.rafael.adapterapplication.fragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.rafael.adapterapplication.R;
import com.example.rafael.adapterapplication.activities.CreateProductActivity;
import com.example.rafael.adapterapplication.adapters.AdapterProducts;
import com.example.rafael.adapterapplication.helper.ValidateInternet;
import com.example.rafael.adapterapplication.models.Product;
import com.example.rafael.adapterapplication.repositories.Repositorio;

import java.io.IOException;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class OneFragment extends Fragment {


    private RecyclerView products_recyclerView;
    private AdapterProducts adapterProducts;
    //private RecyclerView pets_recyclerView;
    //private AdapterPets adapterPets;
    LinearLayoutManager linearLayoutManager;
    private Repositorio repositorio;
    private Button buttonCreateProduct;

    public OneFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_one, container, false);
        buttonCreateProduct = view.findViewById(R.id.buttonCreateProduct);
        loadAdapter(view);
        buttonCreateProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), CreateProductActivity.class);
                startActivity(intent);
            }
        });
        return view;
    }

    private void validateInternet(){
        final ValidateInternet validateInternet = new ValidateInternet(getContext());
        if(validateInternet.isConnected()){
            createThreadGetProduct();
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle(R.string.title_validate_internet);
            alertDialog.setMessage(R.string.message_validate_internet);
            alertDialog.setCancelable(false);
            alertDialog.setPositiveButton(R.string.text_again, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    validateInternet();
                    dialogInterface.dismiss();
                }
            });
            alertDialog.show();
        }
    }

    private void loadAdapter(View view) {
        products_recyclerView = view.findViewById(R.id.products_recyclerView);
        //pets_recyclerView = view.findViewById(R.id.pets_recyclerView);
        //loadProductsAdapter(view);
        //loadPetsAdapter(view);
        repositorio = new Repositorio();
        validateInternet();
    }

    private void getProducts() {
        try {
            ArrayList<Product> productArrayList = repositorio.getProducts();
            loadProductsAdapter(productArrayList);
        } catch (final IOException e) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public void loadProductsAdapter(final ArrayList<Product> products) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapterProducts = new AdapterProducts(products);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
                products_recyclerView.setLayoutManager(linearLayoutManager);
                products_recyclerView.setAdapter(adapterProducts);
            }
        });
    }

    /*public void loadPetsAdapter(View view) {
        Pet pet = new Pet();
        pet.setNombre("Rocky");
        pet.setRaza("ChowChow");
        pet.setEdad("6 Meses");

        ArrayList<Pet> petArrayList = new ArrayList<>();
        petArrayList.add(pet);
        petArrayList.add(pet);
        petArrayList.add(pet);
        petArrayList.add(pet);
        petArrayList.add(pet);
        adapterPets = new AdapterPets(petArrayList);

        linearLayoutManager = new LinearLayoutManager(view.getContext());
        pets_recyclerView.setLayoutManager(linearLayoutManager);
        pets_recyclerView.setAdapter(adapterPets);
    }*/

    private void createThreadGetProduct(){
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                getProducts();
            }
        });
        thread.start();
    }

    @Override
    public void onResume() {
        super.onResume();
        validateInternet();
    }
}