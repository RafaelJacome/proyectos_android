package com.cedesistemas.detailproduct.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.cedesistemas.detailproduct.R;
import com.cedesistemas.detailproduct.helper.ValidateInternet;
import com.cedesistemas.detailproduct.models.Product;
import com.cedesistemas.detailproduct.services.Repository;

import java.io.IOException;

public class DetailActivity extends AppCompatActivity {

    private Product product;

    private TextView tvNombre;
    private TextView tvDescripcion;
    private TextView tvCantidad;
    private TextView tvPrecio;
    private TextView tvMarca;
    private Button btnDelete;

    private ValidateInternet validateInternet;
    private Repository repository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        validateInternet = new ValidateInternet(this);
        repository = new Repository();
        product = (Product) getIntent().getSerializableExtra("product");

        loadView();
        setView(product);
    }

    private void setView(Product product) {
        tvNombre.setText(product.getProductName());
        tvDescripcion.setText(product.getProductDescription());
        tvCantidad.setText(String.valueOf(product.getQuantity()));
        tvPrecio.setText(String.valueOf(product.getPrice()));
        tvMarca.setText(product.getBrand());
    }

    private void loadView() {
        tvNombre = findViewById(R.id.tvNombre);
        tvDescripcion = findViewById(R.id.tvDescripcion);
        tvCantidad = findViewById(R.id.tvCantidad);
        tvPrecio = findViewById(R.id.tvPrecio);
        tvMarca = findViewById(R.id.tvMarca);
        btnDelete = findViewById(R.id.btnDelete);
    }

    public void deleteProduct(View view) {
        if (validateInternet.isConnected()) {
            createThreadDeleteProduct();
        } else {
            Toast.makeText(this, "No hay conexión a INTERNET", Toast.LENGTH_SHORT).show();
        }
    }

    private void createThreadDeleteProduct() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                deleteProductInRepository();
            }
        });
        thread.start();
    }

    private void deleteProductInRepository() {
        try {
            repository.deleteProducts(product.getIdProduct());
            showToast(getResources().getString(R.string.delete_product));
        } catch (IOException e) {
            showToast(e.getMessage());
        }
    }

    private void showToast(final String message){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(DetailActivity.this, message, Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }
}
