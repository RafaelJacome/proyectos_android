package com.example.rafael.fragmentwithtabs;

import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private TabLayout tabs_container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewPager = findViewById(R.id.viewPager);
        tabs_container = findViewById(R.id.tabs_container);
        loadViewPager();
    }

    private void loadViewPager() {
        TabsAdapter tabsAdapter = new TabsAdapter(getSupportFragmentManager());
        viewPager.setAdapter(tabsAdapter);
        tabs_container.setSelectedTabIndicatorColor(
                ContextCompat.getColor(this, R.color.colorWhite));
        tabs_container.setTabTextColors(
                ColorStateList.valueOf(ContextCompat.getColor(this, R.color.colorWhite)));
        tabs_container.setupWithViewPager(viewPager);
    }
}
