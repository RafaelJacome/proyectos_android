package com.example.rafael.futbol.views.interfaces;

import com.example.rafael.futbol.models.Leagues;
import com.example.rafael.futbol.models.Teams;
import com.example.rafael.futbol.views.IBaseView;

public interface ISplashViews extends IBaseView {

    void showAlert();

    void saveTeamList(Leagues leagues, Teams teams);

}
