package com.example.rafael.futbol.views.interfaces;

import com.example.rafael.futbol.models.Team;
import com.example.rafael.futbol.models.Teams;
import com.example.rafael.futbol.views.IBaseView;

public interface IMainViews extends IBaseView {

    void intentToDetailTeam(Team team);

    void showAlert(String idLeague);

    void getTeamList(Teams listTeam);
}
