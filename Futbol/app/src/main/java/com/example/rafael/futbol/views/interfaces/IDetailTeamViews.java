package com.example.rafael.futbol.views.interfaces;

import com.example.rafael.futbol.models.Events;
import com.example.rafael.futbol.views.IBaseView;

public interface IDetailTeamViews extends IBaseView {
    void showAlert(String ideEquipo);

    void showEvent(Events events);
}
