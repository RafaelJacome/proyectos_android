package com.example.rafael.futbol.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class League implements Serializable {

    @SerializedName("idLeague")
    @Expose
    private String idLeague;

    @SerializedName("strLeague")
    @Expose
    private String league;

    @SerializedName("strSport")
    @Expose
    private String sport;

    @SerializedName("strLeagueAlternate")
    @Expose
    private String leagueAlternate;

    public String getIdLeague() {
        return idLeague;
    }

    public String getLeague() {
        return league;
    }

    public String getSport() {
        return sport;
    }

    public String getLeagueAlternate() {
        return leagueAlternate;
    }
}
