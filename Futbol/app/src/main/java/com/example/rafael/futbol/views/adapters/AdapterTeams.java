package com.example.rafael.futbol.views.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.rafael.futbol.R;
import com.example.rafael.futbol.models.Team;
import com.example.rafael.futbol.views.interfaces.IMainViews;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AdapterTeams extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private ArrayList<Team> teamArrayList;
    private IMainViews iMainViews;
    private Context context;

    public AdapterTeams(Context context, ArrayList<Team> teams, IMainViews iMainViews) {
        this.context = context;
        this.teamArrayList = teams;
        this.iMainViews = iMainViews;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_teams,
                viewGroup, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        AdapterTeams.CustomViewHolder customViewHolder = (CustomViewHolder) viewHolder;
        final Team team = teamArrayList.get(position);
        Picasso.get().load(team.getBanner()).into(customViewHolder.imageView_Banner);
        Picasso.get().load(team.getEscudo()).into(customViewHolder.imageView_Shield);
        customViewHolder.textView_Stadium.setText(team.getNombreEstadio());

        customViewHolder.cardViewItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iMainViews.intentToDetailTeam(team);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (teamArrayList != null) ? teamArrayList.size() : 0;
    }

    private class CustomViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageView_Banner;
        private ImageView imageView_Shield;
        private TextView textView_Stadium;
        private CardView cardViewItem;

        CustomViewHolder(@NonNull View itemView) {
            super(itemView);
            initView();
        }

        private void initView() {
            imageView_Banner = itemView.findViewById(R.id.imageView_Banner);
            imageView_Shield = itemView.findViewById(R.id.imageView_Shield);
            textView_Stadium = itemView.findViewById(R.id.textView_Stadium);
            cardViewItem = itemView.findViewById(R.id.cardviewItem);

        }
    }

}
