package com.example.rafael.futbol.presenters;

import com.example.rafael.futbol.models.Events;
import com.example.rafael.futbol.services.Repository;
import com.example.rafael.futbol.views.interfaces.IDetailTeamViews;

import java.io.IOException;

public class DetailTeamPresenter extends BasePresenter<IDetailTeamViews> {

    Repository repository;

    public void getEvent(String idEquipo) {
        if (getValidateInternet().isConnected()) {
            repository = new Repository();
            createThreadGetEvent(String.valueOf(idEquipo));
        } else {
            getView().showAlert(idEquipo);
        }
    }

    public void createThreadGetEvent(final String idEquipo) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                getEventRepo(idEquipo);
            }
        });
        thread.start();
    }

    private void getEventRepo(String idEquipo) {
        try {
            Events events = repository.getEvent(idEquipo);
            getView().showEvent(events);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
