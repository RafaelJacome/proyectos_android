package com.example.rafael.futbol.helper;

public class Constants {
    public final static String OBJECT_LEAGUES = "leagues";
    public final static String OBJECT_TEAMS = "teams";
    public final static String OBJECT_TEAM = "team";

}
