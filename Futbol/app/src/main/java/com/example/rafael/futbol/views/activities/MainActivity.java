package com.example.rafael.futbol.views.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.rafael.futbol.R;
import com.example.rafael.futbol.models.League;
import com.example.rafael.futbol.models.Leagues;
import com.example.rafael.futbol.models.Team;
import com.example.rafael.futbol.models.Teams;
import com.example.rafael.futbol.presenters.MainPresenter;
import com.example.rafael.futbol.views.BaseActivity;
import com.example.rafael.futbol.views.adapters.AdapterTeams;
import com.example.rafael.futbol.views.interfaces.IMainViews;

import java.util.ArrayList;
import java.util.List;

import static com.example.rafael.futbol.helper.Constants.OBJECT_LEAGUES;
import static com.example.rafael.futbol.helper.Constants.OBJECT_TEAM;
import static com.example.rafael.futbol.helper.Constants.OBJECT_TEAMS;

public class MainActivity extends BaseActivity<MainPresenter> implements IMainViews {

    private Leagues leagues;
    private Teams teams;

    private Spinner spinner_leagues;
    private RecyclerView recyclerView_team;
    private AdapterTeams adapterTeams;
    private LinearLayoutManager linearLayoutManager;
    private boolean isFirstTime = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setPresenter(new MainPresenter());
        getPresenter().inject(this, getValidateInternet());
        initViews();
        leagues = (Leagues) getIntent().getSerializableExtra(OBJECT_LEAGUES);
        teams = (Teams) getIntent().getSerializableExtra(OBJECT_TEAMS);
        isFirstTime = true;
        showSpinnerLeagues(leagues.getLeagueArrayList());
        showTeamList(teams);
    }

    private void showSpinnerLeagues(ArrayList<League> leagueArrayList) {
        List<String> spinnerArray = new ArrayList<>();
        for (int i = 0; i < leagueArrayList.size(); i++) {
            spinnerArray.add(leagueArrayList.get(i).getLeague());
        }
        ArrayAdapter<String> adapterSpinner = new ArrayAdapter<String>(MainActivity.this,
                android.R.layout.simple_spinner_item, spinnerArray);
        spinner_leagues.setAdapter(adapterSpinner);
    }

    private void initViews() {
        spinner_leagues = findViewById(R.id.spinner_leagues);
        recyclerView_team = findViewById(R.id.recyclerView_contacts);

        spinner_leagues.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.i("MainActivity: ", leagues.getLeagueArrayList().get((int) id).getIdLeague());
                if (!isFirstTime) {
                    getPresenter().getTeamsList(leagues.getLeagueArrayList().get((int) id).getIdLeague());
                } else {
                    isFirstTime = false;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void showTeamList(final Teams teams) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (teams.getTeams() != null) {
                    adapterTeams = new AdapterTeams(MainActivity.this, teams.getTeams(), MainActivity.this);
                    linearLayoutManager = new LinearLayoutManager(MainActivity.this);
                    recyclerView_team.setLayoutManager(linearLayoutManager);
                    recyclerView_team.setAdapter(adapterTeams);
                } else {
                    Toast.makeText(MainActivity.this, "Equipos no configurados", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void intentToDetailTeam(Team team) {
        Intent intent = new Intent(this, DetailTeamActivity.class);
        intent.putExtra(OBJECT_TEAM, team);
        startActivity(intent);
    }

    @Override
    public void showAlert(final String idLeague) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(R.string.title_validate_internet);
        alertDialog.setMessage(R.string.message_validate_internet);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(R.string.text_again, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                getPresenter().getTeamsList(idLeague);
                dialogInterface.dismiss();
            }
        });
        alertDialog.show();
    }

    @Override
    public void getTeamList(Teams listTeam) {
        this.teams = listTeam;
        showTeamList(teams);
    }
}
