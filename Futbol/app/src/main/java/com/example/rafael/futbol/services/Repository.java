package com.example.rafael.futbol.services;

import com.example.rafael.futbol.models.Events;
import com.example.rafael.futbol.models.Leagues;
import com.example.rafael.futbol.models.Teams;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;


public class Repository {

    private IServices iServices;

    public Repository() {
        ServicesFactory servicesFactory = new ServicesFactory();
        iServices = (IServices) servicesFactory.getInstanceService(IServices.class);
    }

    public Leagues getListLeagues() throws IOException {
        try {
            Call<Leagues> call = iServices.getListLeagues();
            Response<Leagues> response = call.execute();
            if (response.errorBody() != null) {
                throw defaultError();
            } else {
                return response.body();
            }
        } catch (IOException e) {
            throw defaultError();
        }
    }

    public Teams getListTeam(String idLeague) throws IOException {
        try {
            Call<Teams> call = iServices.getListTeam(idLeague);
            Response<Teams> response = call.execute();
            if (response.errorBody() != null) {
                throw defaultError();
            } else {
                return response.body();
            }
        } catch (IOException e) {
            throw defaultError();
        }
    }

    public Events getEvent(String ideEquipo) throws IOException {
        try {
            Call<Events> call = iServices.getEventService(ideEquipo);
            Response<Events> response = call.execute();
            if (response.errorBody() != null) {
                throw defaultError();
            } else {
                return response.body();
            }
        } catch (IOException e) {
            throw defaultError();
        }
    }

    private IOException defaultError() {
        return new IOException("Ha ocurrido un error");
    }

}
