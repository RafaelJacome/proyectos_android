package com.example.rafael.futbol.views.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rafael.futbol.R;
import com.example.rafael.futbol.models.Events;
import com.example.rafael.futbol.models.Team;
import com.example.rafael.futbol.presenters.DetailTeamPresenter;
import com.example.rafael.futbol.views.BaseActivity;
import com.example.rafael.futbol.views.adapters.AdapterEvents;
import com.example.rafael.futbol.views.interfaces.IDetailTeamViews;
import com.squareup.picasso.Picasso;

import static com.example.rafael.futbol.helper.Constants.OBJECT_TEAM;

public class DetailTeamActivity extends BaseActivity<DetailTeamPresenter> implements IDetailTeamViews {

    private ImageView imageView_logo;
    private ImageView imageView_Stadium;
    private ImageView imageView_Uniform;
    private TextView textView_FundationDate;
    private TextView textView_Description;
    private RecyclerView recyclerView_eventos;

    private Team team;
    private AdapterEvents adapterEvents;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_team);

        setPresenter(new DetailTeamPresenter());
        getPresenter().inject(this, getValidateInternet());

        team = (Team) getIntent().getSerializableExtra(OBJECT_TEAM);
        getPresenter().getEvent(team.getIdEquipo());
        initView();
        setView();
    }

    private void initView() {
        imageView_logo = findViewById(R.id.imageView_logo);
        imageView_Stadium = findViewById(R.id.imageView_Stadium);
        imageView_Uniform = findViewById(R.id.imageView_Uniform);
        textView_FundationDate = findViewById(R.id.textView_FundationDate);
        textView_Description = findViewById(R.id.textView_Description);
        recyclerView_eventos = findViewById(R.id.recyclerView_eventos);
    }

    private void setView() {
        Picasso.get().load(team.getLogo()).into(imageView_logo);
        Picasso.get().load(team.getStadiumThumb()).into(imageView_Stadium);
        Picasso.get().load(team.getCamiseta()).into(imageView_Uniform);
        textView_FundationDate.setText(team.getNombre() + "-" + team.getAnoFundacion());
        textView_Description.setText(team.getDescripcion());
        textView_Description.setMovementMethod(new ScrollingMovementMethod());
    }

    @Override
    public void showAlert(final String ideEquipo) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(R.string.title_validate_internet);
        alertDialog.setMessage(R.string.message_validate_internet);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(R.string.text_again, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                getPresenter().getEvent(ideEquipo);
                dialogInterface.dismiss();
            }
        });
        alertDialog.show();
    }

    @Override
    public void showEvent(final Events eventos) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (eventos.getEvent() != null) {
                    adapterEvents = new AdapterEvents(eventos.getEvent(), DetailTeamActivity.this);
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(DetailTeamActivity.this);
                    recyclerView_eventos.setLayoutManager(linearLayoutManager);
                    recyclerView_eventos.setAdapter(adapterEvents);
                } else {
                    Toast.makeText(DetailTeamActivity.this, "No tiene eventos configurados", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void openSite(View view) {
        String urlSite = "";
        switch (view.getId()) {
            case R.id.button_website:
                urlSite = team.getWebsite();
                break;

            case R.id.button_facebook:
                urlSite = team.getFacebook();
                break;

            case R.id.button_instagram:
                urlSite = team.getInstagram();
                break;

            case R.id.button_twitter:
                urlSite = team.getTwitter();
                break;

            case R.id.button_youtube:
                urlSite = team.getYoutube();
                break;
        }
        if (urlSite.isEmpty()) {
            Toast.makeText(this, "No tiene sitio web configurado", Toast.LENGTH_SHORT).show();
        } else {
            openWebSites(urlSite);
        }

    }
}
