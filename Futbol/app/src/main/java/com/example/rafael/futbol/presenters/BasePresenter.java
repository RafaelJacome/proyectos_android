package com.example.rafael.futbol.presenters;

import com.example.rafael.futbol.helper.ValidateInternet;
import com.example.rafael.futbol.views.IBaseView;

public class BasePresenter<T extends IBaseView> {

    private ValidateInternet validateInternet;
    private T view;

    public void inject(T view, ValidateInternet validateInternet) {
        this.validateInternet = validateInternet;
        this.view = view;
    }

    public ValidateInternet getValidateInternet() {
        return validateInternet;
    }

    public T getView() {
        return view;
    }

}
