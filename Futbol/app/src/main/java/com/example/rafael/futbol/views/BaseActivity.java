package com.example.rafael.futbol.views;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.example.rafael.futbol.helper.ValidateInternet;
import com.example.rafael.futbol.models.Team;
import com.example.rafael.futbol.presenters.BasePresenter;

import java.util.ArrayList;

public class BaseActivity<T extends BasePresenter> extends AppCompatActivity implements IBaseView {

    private ValidateInternet validateInternet;
    private T presenter;
    private ArrayList<Team> team;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        validateInternet = new ValidateInternet(this);
    }

    public ValidateInternet getValidateInternet() {
        return validateInternet;
    }

    public T getPresenter() {
        return presenter;
    }

    public void setPresenter(T presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showToast(int result) {
        Toast.makeText(this, String.valueOf(result) + " BaseActivity", Toast.LENGTH_SHORT).show();
    }

    public void openWebSites(String webSite) {
        String url = "http://" + webSite;
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        startActivity(intent);
    }


}

