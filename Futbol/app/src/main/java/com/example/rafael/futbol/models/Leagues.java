package com.example.rafael.futbol.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class Leagues implements Serializable {

    @SerializedName("leagues")
    @Expose
    private ArrayList<League> leagueArrayList;

    public ArrayList<League> getLeagueArrayList() {
        return leagueArrayList;
    }
}
