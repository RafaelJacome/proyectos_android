package com.example.rafael.futbol.presenters;

import com.example.rafael.futbol.models.Teams;
import com.example.rafael.futbol.services.Repository;
import com.example.rafael.futbol.views.interfaces.IMainViews;

import java.io.IOException;

public class MainPresenter extends BasePresenter<IMainViews> {

    private Repository repository;

    public void getTeamsList(String idLeague) {
        if (getValidateInternet().isConnected()) {
            repository = new Repository();
            createThreadGetListTeams(idLeague);
        } else {
            getView().showAlert(idLeague);
        }
    }


    public void createThreadGetListTeams(final String idLeague) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                getListTeams(idLeague);
            }
        });
        thread.start();
    }

    private void getListTeams(String idLeague) {
        try {
            Teams listTeam = repository.getListTeam(idLeague);
            getView().getTeamList(listTeam);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
