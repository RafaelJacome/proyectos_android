package com.example.rafael.futbol.presenters;

import com.example.rafael.futbol.models.Leagues;
import com.example.rafael.futbol.models.Teams;
import com.example.rafael.futbol.services.Repository;
import com.example.rafael.futbol.views.interfaces.ISplashViews;

import java.io.IOException;

public class SplashPresenter extends BasePresenter<ISplashViews> {

    private Repository repository;
    private Leagues listLeagues;

    public void getLeagueList() {
        if (getValidateInternet().isConnected()) {
            repository = new Repository();
            createThreadGetListLeagues();
        } else {
            getView().showAlert();
        }
    }

    private void createThreadGetListLeagues() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                getListLeagues();
            }
        });
        thread.start();
    }

    private void getListLeagues() {
        try {
            listLeagues = repository.getListLeagues();
            getTeamList(listLeagues.getLeagueArrayList().get(0).getIdLeague());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void getTeamList(String idLeague) {
        if (getValidateInternet().isConnected()) {
            repository = new Repository();
            createThreadGetListTeams(idLeague);
        } else {
            getView().showAlert();
        }
    }


    public void createThreadGetListTeams(final String idLeague) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                getListTeams(idLeague);
            }
        });
        thread.start();
    }

    private void getListTeams(String idLeague) {
        try {
            Teams listTeam = repository.getListTeam(idLeague);
            getView().saveTeamList(listLeagues, listTeam);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
