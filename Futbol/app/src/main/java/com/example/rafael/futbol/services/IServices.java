package com.example.rafael.futbol.services;

import com.example.rafael.futbol.models.Events;
import com.example.rafael.futbol.models.Leagues;
import com.example.rafael.futbol.models.Teams;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface IServices {

    @GET("all_leagues.php")
    Call<Leagues> getListLeagues();

    @GET("lookup_all_teams.php")
    Call<Teams> getListTeam(@Query("id") String idLeague);

    @GET("eventsnext.php")
    Call<Events> getEventService(@Query("id") String idEquipo);


}
