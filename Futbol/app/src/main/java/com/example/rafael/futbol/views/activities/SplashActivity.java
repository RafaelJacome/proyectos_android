package com.example.rafael.futbol.views.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.example.rafael.futbol.R;
import com.example.rafael.futbol.models.Leagues;
import com.example.rafael.futbol.models.Teams;
import com.example.rafael.futbol.presenters.SplashPresenter;
import com.example.rafael.futbol.views.BaseActivity;
import com.example.rafael.futbol.views.interfaces.ISplashViews;

import static com.example.rafael.futbol.helper.Constants.OBJECT_LEAGUES;
import static com.example.rafael.futbol.helper.Constants.OBJECT_TEAMS;

public class SplashActivity extends BaseActivity<SplashPresenter> implements ISplashViews {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        setPresenter(new SplashPresenter());
        getPresenter().inject(this, getValidateInternet());
        getPresenter().getLeagueList();
        //getPresenter().getTeamList();
    }

    private void intentToMainActivity(Leagues leagues, Teams teams) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(OBJECT_LEAGUES, leagues);
        intent.putExtra(OBJECT_TEAMS, teams);
        startActivity(intent);
    }


    @Override
    public void showAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(R.string.title_validate_internet);
        alertDialog.setMessage(R.string.message_validate_internet);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(R.string.text_again, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                getPresenter().getLeagueList();
                dialogInterface.dismiss();
            }
        });
        alertDialog.show();
    }

    @Override
    public void saveTeamList(final Leagues leagues, final Teams teams) {
        intentToMainActivity(leagues, teams);
        finish();
    }
}
