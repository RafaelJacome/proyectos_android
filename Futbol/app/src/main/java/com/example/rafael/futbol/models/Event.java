package com.example.rafael.futbol.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Event {

    @SerializedName("strFilename")
    @Expose
    private String fec_evento;

    public String getFec_evento() {
        return fec_evento;
    }

    public void setFec_evento(String fec_evento) {
        this.fec_evento = fec_evento;
    }
}
