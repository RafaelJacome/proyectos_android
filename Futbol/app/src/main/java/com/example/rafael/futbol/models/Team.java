package com.example.rafael.futbol.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Team implements Serializable {

    @SerializedName("idTeam")
    @Expose
    private String idEquipo;

    @SerializedName("strTeam")
    @Expose
    private String nombre;

    @SerializedName("strStadium")
    @Expose
    private String nombreEstadio;

    @SerializedName("strTeamBadge")
    @Expose
    private String escudo;

    @SerializedName("strDescriptionES")
    @Expose
    private String descripcion;

    @SerializedName("intFormedYear")
    @Expose
    private String anoFundacion;

    @SerializedName("strTeamJersey")
    @Expose
    private String camiseta;

    @SerializedName("strWebsite")
    @Expose
    private String website;

    @SerializedName("strFacebook")
    @Expose
    private String facebook;

    @SerializedName("strTwitter")
    @Expose
    private String twitter;

    @SerializedName("strInstagram")
    @Expose
    private String instagram;

    @SerializedName("strYoutube")
    @Expose
    private String youtube;

    @SerializedName("strTeamLogo")
    @Expose
    private String logo;

    @SerializedName("strTeamBanner")
    @Expose
    private String banner;

    @SerializedName("strStadiumThumb")
    @Expose
    private String stadiumThumb;

    public String getIdEquipo() {
        return idEquipo;
    }

    public String getNombre() {
        return nombre;
    }

    public String getNombreEstadio() {
        return nombreEstadio;
    }

    public String getEscudo() {
        return escudo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getAnoFundacion() {
        return anoFundacion;
    }

    public String getCamiseta() {
        return camiseta;
    }

    public String getWebsite() {
        return website;
    }

    public String getFacebook() {
        return facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public String getInstagram() {
        return instagram;
    }

    public String getYoutube() {
        return youtube;
    }

    public String getLogo() {
        return logo;
    }

    public String getBanner() {
        return banner;
    }

    public String getStadiumThumb() {
        return stadiumThumb;
    }
}
