package com.example.rafael.futbol.views.adapters;

import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.rafael.futbol.R;
import com.example.rafael.futbol.models.Event;
import com.example.rafael.futbol.views.interfaces.IDetailTeamViews;

import java.util.ArrayList;

public class AdapterEvents extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Event> eventArrayList;
    private IDetailTeamViews iDetailTeamViews;

    public AdapterEvents(ArrayList<Event> eventArrayList, IDetailTeamViews iDetailTeamViews) {
        this.eventArrayList = eventArrayList;
        this.iDetailTeamViews = iDetailTeamViews;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_events,
                viewGroup, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        AdapterEvents.CustomViewHolder customViewHolder = (CustomViewHolder) viewHolder;
        Event event = eventArrayList.get(position);
        String auxTextView = (position + 1) + "). " + event.getFec_evento();
        customViewHolder.textView_event.setText(auxTextView);
    }

    @Override
    public int getItemCount() {
        return (eventArrayList != null) ? eventArrayList.size() : 0;
    }

    private class CustomViewHolder extends RecyclerView.ViewHolder {

        private TextView textView_event;

        public CustomViewHolder(@NonNull View itemView) {
            super(itemView);
            textView_event = itemView.findViewById(R.id.textView_event);
        }
    }
}
