package com.example.rafael.myapplication;

public class Constants {
    public final static String USER_NAME = "user";
    public final static String USER_OBJECT = "object";
    public final static String ORDER_OBJECT = "order_object";
    public final static String CONFIRMATION = "confirmation";
    public final static int RESULT_OK = 1;
}
