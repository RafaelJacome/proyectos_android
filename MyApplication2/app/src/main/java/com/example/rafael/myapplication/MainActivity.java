package com.example.rafael.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import static com.example.rafael.myapplication.Constants.*;


public class MainActivity extends AppCompatActivity implements TextWatcher {

    private EditText etUser;
    private EditText etPassword;
    private Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Se obtienen los datos de la vista
        etUser = findViewById(R.id.etUser);
        etPassword = findViewById(R.id.etPassword);
        btnLogin = findViewById(R.id.btnLogin);

        //Se adiciona escuchador
        etUser.addTextChangedListener(this);
        etPassword.addTextChangedListener(this);

        //Cuando se presiona CLICK en el boton (Opcion 1)
        //La Opcion 2 es desde la vista (Metodo Login)
       /* btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MenuActivity.class);
                startActivity(intent);
            }
        });*/


    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (etUser.getText().toString().trim().isEmpty() ||
                etPassword.getText().toString().trim().isEmpty()) {
            btnLogin.setEnabled(false);
        } else {
            btnLogin.setEnabled(true);
        }
    }

    //Opcion 2
    public void login(View view) {
        User user = new User();

        Intent intent = new Intent(MainActivity.this, ComponentesActivity.class);
        intent.putExtra(USER_NAME, etUser.getText().toString());
        intent.putExtra(USER_OBJECT, user);
        startActivity(intent);
    }
}
