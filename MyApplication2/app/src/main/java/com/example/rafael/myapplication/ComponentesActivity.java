package com.example.rafael.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import static com.example.rafael.myapplication.Constants.*;

public class ComponentesActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private String TAG = "ComponentesActivity";

    private TextView txtFoodSelected;
    private TextView txtAdded;

    private Order order;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_componentes);

        order = new Order();

        Log.i(TAG, "on Create");
        txtFoodSelected = findViewById(R.id.txt_food_selected);
        txtAdded = findViewById(R.id.added_text);
        loadDataFoodList();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, "on Start");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "on Resume");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(TAG, "on Restart");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "on Pause");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "on Destroy");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(resultCode == RESULT_OK){
            Toast.makeText(this, "Recibiendo...", Toast.LENGTH_LONG).show();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void loadDataFoodList() {
        Spinner spFoodList = findViewById(R.id.spinner_food_list);
        spFoodList.setOnItemSelectedListener(this);
        ArrayAdapter<CharSequence> foodAdapter = ArrayAdapter.createFromResource(this,
                R.array.food_list, android.R.layout.simple_spinner_item);
        foodAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spFoodList.setAdapter(foodAdapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
        String foodSelected = (String) adapterView.getItemAtPosition(position);
        Log.i(TAG, foodSelected);
        txtFoodSelected.setText(foodSelected);
        order.setFood(foodSelected);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        //TODO
    }

    public void onCheckBoxClicked(View view) {
        boolean checked = ((CheckBox) view).isChecked();
        String added = checked ? "Con " : "Sin ";

        switch (view.getId()) {
            case R.id.soda_checkbox:
                Log.i(TAG, added + "Soda");
                txtAdded.setText(added + "Soda");
                order.setHasSoda(checked);
                break;
            case R.id.snack_checkbox:
                Log.i(TAG, added + "Snack");
                txtAdded.setText(added + "Snack");
                order.setHasSnack(checked);
                break;
        }

    }

    public void onOrderButtonClicked(View view) {
        Intent intent = new Intent(ComponentesActivity.this, OrderActivity.class);
        intent.putExtra(ORDER_OBJECT,order);
        startActivityForResult(intent, RESULT_OK);
    }
}
