package com.example.rafael.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import static com.example.rafael.myapplication.Constants.*;

public class OrderActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        Order order = (Order) getIntent().getSerializableExtra(ORDER_OBJECT);
        showToastMessage(order);
    }

    private void showToastMessage(Order order){
        Toast.makeText(this, "La orden es: " + order.toString()
                , Toast.LENGTH_LONG).show();
    }


    public void onBack(View view) {
        //1 Option (Using finish method)
        //finish();
        //2 Option (Retornando valores)
        Intent intent = new Intent();
        intent.putExtra(CONFIRMATION, true);
        setResult(RESULT_OK);
        finish();
    }

    //3 Option (Utilizando el back del device)
    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra(CONFIRMATION, true);
        setResult(RESULT_OK);
        super.onBackPressed();
    }
}
