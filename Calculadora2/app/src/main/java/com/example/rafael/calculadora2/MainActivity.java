package com.example.rafael.calculadora2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button btnMultiplicacion;
    private Button btnDivision;
    private Button btnResta;
    private Button btnC;
    private Button btnIgualdad;
    private Button btnSuma;
    private Button btn9;
    private Button btn8;
    private Button btn7;
    private Button btn6;
    private Button btn5;
    private Button btn4;
    private Button btn3;
    private Button btn2;
    private Button btn1;
    private Button btn0;
    private TextView tvResultado;

    private float resultado = 0L;
    private String operacion = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnMultiplicacion = findViewById(R.id.btnMultiplicacion);
        btnDivision = findViewById(R.id.btnDivision);
        btnResta = findViewById(R.id.btnResta);
        btnC = findViewById(R.id.btnC);
        btnIgualdad = findViewById(R.id.btnIgualdad);
        btnSuma = findViewById(R.id.btnSuma);
        btn9 = findViewById(R.id.btn9);
        btn8 = findViewById(R.id.btn8);
        btn7 = findViewById(R.id.btn7);
        btn6 = findViewById(R.id.btn6);
        btn5 = findViewById(R.id.btn5);
        btn4 = findViewById(R.id.btn4);
        btn3 = findViewById(R.id.btn3);
        btn2 = findViewById(R.id.btn2);
        btn1 = findViewById(R.id.btn1);
        btn0 = findViewById(R.id.btn0);
        tvResultado = findViewById(R.id.tvResultado);

    }

    public void botonC(View view) {
        tvResultado.setText("0");
    }

    public void multiplicacion(View view) {
        setValue();
        operacion = "*";
    }

    public void botonDivision(View view) {
        setValue();
        operacion = "/";
    }

    public void botonResta(View view) {
        setValue();
        operacion = "-";
    }

    public void botonSuma(View view) {
        setValue();
        operacion = "+";
    }

    public void botonIgualdad(View view) {
        if (resultado != 0) {
            switch (operacion) {
                case "+":
                    resultado += Float.parseFloat(tvResultado.getText().toString());
                    break;

                case "-":
                    resultado -= Float.parseFloat(tvResultado.getText().toString());
                    break;

                case "/":
                    if (Float.parseFloat(tvResultado.getText().toString()) != 0) {
                        resultado /= Float.parseFloat(tvResultado.getText().toString());
                    } else {
                        Toast.makeText(this, "No se puede dividir por CERO (0)", Toast.LENGTH_LONG).show();
                    }
                    break;

                case "*":
                    resultado *= Float.parseFloat(tvResultado.getText().toString());
                    break;

                default:
                    break;
            }
            tvResultado.setText(String.valueOf(resultado));
        } else {
            tvResultado.setText(String.valueOf(resultado));
        }
        operacion = "=";
    }

    public void boton9(View view) {
        concatDigit("9");
    }

    public void boton8(View view) {
        concatDigit("8");
    }

    public void boton7(View view) {
        concatDigit("7");
    }

    public void boton6(View view) {
        concatDigit("6");
    }

    public void boton5(View view) {
        concatDigit("5");
    }

    public void boton4(View view) {
        concatDigit("4");
    }

    public void boton3(View view) {
        concatDigit("3");
    }

    public void boton2(View view) {
        concatDigit("2");
    }

    public void boton1(View view) {
        concatDigit("1");
    }

    public void boton0(View view) {
        concatDigit("0");
    }

    private void setValue() {
        resultado = Float.parseFloat(tvResultado.getText().toString());
        tvResultado.setText("");
    }

    public void concatDigit(String resultado) {
        if (operacion != "=") {
            tvResultado.setText(tvResultado.getText().toString() + resultado);
        } else {
            tvResultado.setText(resultado);
        }
    }
}
