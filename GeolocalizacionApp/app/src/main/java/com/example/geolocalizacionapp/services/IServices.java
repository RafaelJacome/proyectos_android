package com.example.geolocalizacionapp.services;

import com.example.geolocalizacionapp.Models.Cinemas;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;

public interface IServices {

    @GET("cinemas")
    Call<ArrayList<Cinemas>> getListCinemas();

}
