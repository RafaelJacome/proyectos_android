package com.example.geolocalizacionapp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

public class LocationActivity extends AppCompatActivity {

    private static final int REQUEST_PERMISSION_CODE = 34; //Este numero puede ser cualquiera
    protected Location mLastLocation;
    //La m de la variable se le añade para informar que es un miembro privado
    private TextView mLatitud_textView;
    private TextView mLongitud_textView;
    private FusedLocationProviderClient mFusedLocationProviderClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        mLatitud_textView = findViewById(R.id.latitud_textView);
        mLongitud_textView = findViewById(R.id.longitud_textView);
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(LocationActivity.this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (!checkPermissions()) {
            requestPermissions();
        } else {
            getLastLocation();
        }
    }

    @SuppressLint("MissingPermission") //Se suprime ya que antes del llamado a este metodo ya se está controlando
    private void getLastLocation() {
        mFusedLocationProviderClient.getLastLocation()
                .addOnCompleteListener(LocationActivity.this, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            mLastLocation = task.getResult();
                            mLatitud_textView.setText(String.valueOf(mLastLocation.getLatitude()));
                            mLongitud_textView.setText(String.valueOf(mLastLocation.getLongitude()));
                        } else {
                            Toast.makeText(LocationActivity.this, "Localización no encontrada", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(LocationActivity.this,
                Manifest.permission.ACCESS_COARSE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        boolean shouldProviderRationale = ActivityCompat.
                shouldShowRequestPermissionRationale(LocationActivity.this,
                        Manifest.permission.ACCESS_COARSE_LOCATION);

        if (shouldProviderRationale) {
            //TODO Solicitar al usuario encender el GPS
            Log.i("Location", "Mensaje para el usuario de que encieda el gps");
        } else {
            startLocationPermissionRequest();
        }
    }

    private void startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(LocationActivity.this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                REQUEST_PERMISSION_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_PERMISSION_CODE) {
            if (grantResults.length <= 0) { //El usuario cancela el acceso al permiso solicitado
                Log.i("Location", "El usuario canceló el permiso");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLastLocation();
            } else {
                Log.i("Location", "El usuario denegó el permiso");
            }
        }
    }
}
