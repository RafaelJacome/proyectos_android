package com.example.geolocalizacionapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.example.geolocalizacionapp.Models.Cinemas;
import com.example.geolocalizacionapp.Models.LocationsList;
import com.example.geolocalizacionapp.services.Repository;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.IOException;
import java.util.ArrayList;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private Repository repository;
    ArrayList<Cinemas> cinemas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(
                MapsActivity.this, R.raw.style_map)
        );

        getCinemaList();
    }

    private void getCinemaList() {
        repository = new Repository();
        createThreadGetListCinemas();
    }

    private void createThreadGetListCinemas() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    cinemas = repository.getListCinemas();
                    createMarkers(cinemas);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

    private void showMessages(final ArrayList<Cinemas> cinemas) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.i("MapsActivity:: ", "Llega");
            }
        });
    }


    private void createMarkers(final ArrayList<Cinemas> cinemas) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ArrayList<LatLng> points = new ArrayList<>();
                int sizeCinemas = cinemas.size();
                boolean isOptimize = (sizeCinemas > 1);
                for (int i = 0; i < sizeCinemas; i++) {
                    ArrayList<LocationsList> locationList = cinemas.get(i).getLocationsList();
                    for (int j = 0; j < locationList.size(); j++) {
                        String longitud = locationList.get(j).getLocation().getCoordinates().get(0);
                        String latidud = locationList.get(j).getLocation().getCoordinates().get(1);
                        String nameLocation = locationList.get(j).getNameLocation();
                        LatLng pointCinema = new LatLng(Double.parseDouble(latidud), Double.parseDouble(longitud));
                        mMap.addMarker(new MarkerOptions()
                                .position(pointCinema)
                                .title(nameLocation)
                                .icon(bitmapDescriptorFromVector(MapsActivity.this,
                                        R.drawable.ic_local_movies_black_24dp)));
                        points.add(pointCinema);
                    }
                }

                //mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(points, 18));
                //mMap.addPolyline(new PolylineOptions().add(miCasa, ocatvaMaravilla).width(4).color(Color.GREEN));

                calculateRoute(points, isOptimize);
                centerPoints(points);
            }
        });
    }


    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorId);
        vectorDrawable.setBounds(
                0,
                0,
                vectorDrawable.getIntrinsicWidth(),
                vectorDrawable.getIntrinsicHeight());

        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(),
                vectorDrawable.getIntrinsicHeight(),
                Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }


    private void centerPoints(ArrayList<LatLng> points) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        for (int i = 0; i < points.size(); i++) {
            builder.include(points.get(i));
        }

        LatLngBounds bounds = builder.build();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 50);
        mMap.animateCamera(cameraUpdate);
    }

    RoutingListener routingListener = new RoutingListener() {
        @Override
        public void onRoutingFailure(RouteException e) {
            Log.e("MapsActivity ", e.getMessage());
        }

        @Override
        public void onRoutingStart() {
            Log.i("MapsActivity ", "Iniciando route");
        }

        @Override
        public void onRoutingSuccess(ArrayList<Route> routes, int index) {
            ArrayList<Polyline> polylineArrayList = new ArrayList<>();
            for (int i = 0; i < routes.size(); i++) {
                PolylineOptions polylineOptions = new PolylineOptions();
                polylineOptions.color(ContextCompat
                        .getColor(getApplicationContext(), R.color.colorPrimary));
                polylineOptions.width(5);
                polylineOptions.addAll(routes.get(i).getPoints());

                Polyline polyline = mMap.addPolyline(polylineOptions);
                polylineArrayList.add(polyline);

                int distance = routes.get(i).getDistanceValue();
                int duration = routes.get(i).getDurationValue();

                Log.i("MapsActivity", " " + distance);
                Log.i("MapsActivity", " " + duration);

            }
        }

        @Override
        public void onRoutingCancelled() {

        }
    };

    private void calculateRoute(ArrayList<LatLng> points, boolean isOptimize) {
        Routing routing = new Routing.Builder()
                .travelMode(AbstractRouting.TravelMode.DRIVING)
                .waypoints(points)
                .key(getString(R.string.google_maps_key))
                .optimize(isOptimize)
                .withListener(routingListener)
                .build();
        routing.execute();
    }

}
