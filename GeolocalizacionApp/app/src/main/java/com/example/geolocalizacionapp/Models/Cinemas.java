package com.example.geolocalizacionapp.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class Cinemas implements Serializable {

    @SerializedName("_id")
    @Expose
    private String idCinema;

    @SerializedName("name")
    @Expose
    private String nameCinema;

    @SerializedName("locationsList")
    @Expose
    private ArrayList<LocationsList> locationsList;

    public String getIdCinema() {
        return idCinema;
    }

    public void setIdCinema(String idCinema) {
        this.idCinema = idCinema;
    }

    public String getNameCinema() {
        return nameCinema;
    }

    public void setNameCinema(String nameCinema) {
        this.nameCinema = nameCinema;
    }

    public ArrayList<LocationsList> getLocationsList() {
        return locationsList;
    }

    public void setLocationsList(ArrayList<LocationsList> locationsList) {
        this.locationsList = locationsList;
    }

}
