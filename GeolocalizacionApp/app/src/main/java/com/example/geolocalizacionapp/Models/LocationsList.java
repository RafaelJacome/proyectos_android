package com.example.geolocalizacionapp.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class LocationsList implements Serializable {

    @SerializedName("name")
    @Expose
    private String nameLocation;


    @SerializedName("_id")
    @Expose
    private String idLocation;


    @SerializedName("location")
    @Expose
    private Location location; //Longitud, Latitud

    public String getNameLocation() {
        return nameLocation;
    }

    public void setNameLocation(String nameLocation) {
        this.nameLocation = nameLocation;
    }

    public String getIdLocation() {
        return idLocation;
    }

    public void setIdLocation(String idLocation) {
        this.idLocation = idLocation;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
