package com.example.geolocalizacionapp.services;

import com.example.geolocalizacionapp.Models.Cinemas;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;


public class Repository {

    private IServices iServices;

    public Repository() {
        ServicesFactory servicesFactory = new ServicesFactory();
        iServices = (IServices) servicesFactory.getInstanceService(IServices.class);
    }

    public ArrayList<Cinemas> getListCinemas() throws IOException {
        try {
            Call<ArrayList<Cinemas>> call = iServices.getListCinemas();
            Response<ArrayList<Cinemas>> response = call.execute();
            if (response.errorBody() != null) {
                throw defaultError();
            } else {
                return response.body();
            }
        } catch (IOException e) {
            throw defaultError();
        }
    }

    private IOException defaultError() {
        return new IOException("Ha ocurrido un error");
    }

}
