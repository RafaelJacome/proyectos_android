package com.example.rafael.arquitecturamvp.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;

import com.example.rafael.arquitecturamvp.App;

public class NetworkStateReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        boolean isConected;
        ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo == null || networkInfo.getState() != NetworkInfo.State.CONNECTED) {
            isConected = false;
        } else {
            isConected = networkInfo.isConnected();
        }
        waitAndCheckConnection(isConected, context);
    }

    private void waitAndCheckConnection(final boolean isConected, final Context context) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                App application = (App) context.getApplicationContext();

                if(application != null){
                    application.onNetworkStateChanged(isConected);
                }
            }
        };
        new Handler().postDelayed(runnable, 2000);
    }
}
