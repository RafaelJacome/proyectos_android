package com.example.rafael.arquitecturamvp.providers;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public abstract class DbContentProvider {

    public SQLiteDatabase mDb;

    public DbContentProvider(SQLiteDatabase mDb) {
        this.mDb = mDb;
    }

    protected abstract <T> T cursorToEntity(Cursor cursor);

    public int delete(String tableName, String selection, String[] selectionArgs) {
        return mDb.delete(tableName, selection, selectionArgs);
    }

    public long insert(String tableName, ContentValues contentValues) {
        return mDb.insert(tableName, null, contentValues);
    }

    public long update(String tableName, ContentValues contentValues, String selection,
                       String[] selectionArgs) {
        return mDb.update(tableName, contentValues, selection, selectionArgs);
    }

    public Cursor query(String tableName, String[] columns, String selection,
                        String[] selectionArgs, String sortOrder) {
        return mDb.query(tableName, columns, selection, selectionArgs, null, null,
                sortOrder);
    }

    public Cursor query(String tableName, String[] columns, String selection,
                        String[] selectionArgs, String sortOrder, String limit) {
        return mDb.query(tableName, columns, selection, selectionArgs, null, null,
                sortOrder, limit);
    }

    public Cursor query(String tableName, String[] columns, String selection,
                        String[] selectionArgs, String groupBy, String having, String sortOrder,
                        String limit) {
        return mDb.query(tableName, columns, selection, selectionArgs, groupBy, having, sortOrder,
                limit);
    }
}