package com.example.rafael.arquitecturamvp.helper;

public class Constants {

    public final static String USER_OBJECT = "object";
    public final static String TOKEN = "token";
    public final static String USER = "user";
    public static final String DATABASE_NAME = "db_contacts.db";
    public static final int DATABASE_VERSION = 1;
}
