package com.example.rafael.arquitecturamvp.views;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.example.rafael.arquitecturamvp.helper.ValidateInternet;
import com.example.rafael.arquitecturamvp.presenters.BasePresenter;

public class BaseActivity<T extends BasePresenter> extends AppCompatActivity implements IBaseView {

    private ValidateInternet validateInternet;
    private T presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        validateInternet = new ValidateInternet(this);
    }

    public ValidateInternet getValidateInternet() {
        return validateInternet;
    }

    public T getPresenter() {
        return presenter;
    }

    public void setPresenter(T presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showToast(int result) {
        Toast.makeText(this, String.valueOf(result) + " BaseActivity", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}

