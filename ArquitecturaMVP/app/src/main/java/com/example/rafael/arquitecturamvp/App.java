package com.example.rafael.arquitecturamvp;

import android.app.Application;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.util.Log;

import com.example.rafael.arquitecturamvp.helper.Database;
import com.example.rafael.arquitecturamvp.receivers.NetworkStateReceiver;

import java.io.BufferedOutputStream;

public class App extends Application {

    public static Database mDb;
    private final NetworkStateReceiver NETWORK_STATE_RECEIVER = new NetworkStateReceiver();

    @Override
    public void onCreate() {
        super.onCreate();
        createDatabase();
        registerNetworkStateReceiver();
    }

    private void registerNetworkStateReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(NETWORK_STATE_RECEIVER, intentFilter);
    }

    private void createDatabase() {
        mDb = new Database(this);
        mDb.open();
    }

    @Override
    public void onTerminate() {
        mDb.close();
        super.onTerminate();
    }

    public void onNetworkStateChanged(boolean isConected) {
        //TODO
        String msg = isConected ? "Conectado" : "Desconectado";
        Log.i("CONNECTION_STATE", msg);
    }
}
