package com.example.rafael.arquitecturamvp.views.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.rafael.arquitecturamvp.R;
import com.example.rafael.arquitecturamvp.models.Contact;

import java.util.ArrayList;

public class AdapterContacts extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<Contact> contactArrayList;

    public AdapterContacts(ArrayList<Contact> contactArrayList) {
        this.contactArrayList = contactArrayList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_contacts,
                viewGroup, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        AdapterContacts.CustomViewHolder customViewHolder = (AdapterContacts.CustomViewHolder) viewHolder;
        Contact contact = contactArrayList.get(position);
        customViewHolder.textView_Name.setText(contact.getName());
        customViewHolder.textView_Phone.setText(contact.getPhone());
        customViewHolder.textView_Company.setText(contact.getCompany());
    }

    @Override
    public int getItemCount() {
        return contactArrayList.size();
    }

    private class CustomViewHolder extends RecyclerView.ViewHolder {

        private TextView textView_Name;
        private TextView textView_Phone;
        private TextView textView_Company;

        public CustomViewHolder(@NonNull View itemView) {
            super(itemView);
            initView();
        }

        private void initView() {
            textView_Name = itemView.findViewById(R.id.textView_Name);
            textView_Phone = itemView.findViewById(R.id.textView_Phone);
            textView_Company = itemView.findViewById(R.id.textView_Company);
        }
    }
}
