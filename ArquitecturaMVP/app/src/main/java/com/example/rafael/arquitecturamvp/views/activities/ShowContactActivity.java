package com.example.rafael.arquitecturamvp.views.activities;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.rafael.arquitecturamvp.R;
import com.example.rafael.arquitecturamvp.models.Contact;
import com.example.rafael.arquitecturamvp.presenters.ShowContactPresenter;
import com.example.rafael.arquitecturamvp.views.BaseActivity;
import com.example.rafael.arquitecturamvp.views.adapters.AdapterContacts;
import com.example.rafael.arquitecturamvp.views.interfaces.IShowContactsView;

import java.util.ArrayList;

public class ShowContactActivity extends BaseActivity<ShowContactPresenter> implements IShowContactsView {

    private RecyclerView recyclerView_contacts;
    private AdapterContacts adapterContacts;
    LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_contact);

        setPresenter(new ShowContactPresenter());
        getPresenter().inject(this, getValidateInternet());

        initViews();
        getPresenter().showContactList();
    }

    private void loadContactsAdapter(ArrayList<Contact> contactArrayList) {
        adapterContacts = new AdapterContacts(contactArrayList);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView_contacts.setLayoutManager(linearLayoutManager);
        recyclerView_contacts.setAdapter(adapterContacts);
    }

    private void initViews() {
        recyclerView_contacts = findViewById(R.id.recyclerView_contacts);
    }


    @Override
    public void getContactList(ArrayList<Contact> contactArrayList) {
        loadContactsAdapter(contactArrayList);
    }
}
