package com.example.rafael.arquitecturamvp.schemes;

public interface IContactosScheme {

    String CONTACTS_TABLE = "contactos";
    String COLUMN_ID = "id";
    String COLUMN_NAME = "name";
    String COLUMN_PHONE = "phone";
    String COLUMN_COMPANY = "company";

    //Script para la creacion de la tabla
    String CONTACTS_TABLE_CREATE = "CREATE TABLE IF NOT EXISTS "
            + CONTACTS_TABLE + " ( "
            + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
            + COLUMN_NAME + " TEXT NOT NULL, "
            + COLUMN_PHONE + " TEXT NOT NULL, "
            + COLUMN_COMPANY + " TEXT"
            + " );";

    //Lista de columnas de la tabla
    String[] CONTACTS_COLUMNS = new String[] {
            COLUMN_ID,
            COLUMN_NAME,
            COLUMN_PHONE,
            COLUMN_COMPANY
    };

}
