package com.example.rafael.arquitecturamvp.views.interfaces;

import com.example.rafael.arquitecturamvp.views.IBaseView;

public interface IMainViews extends IBaseView{
    //opcion 2
    void showResult(int result);

    void showMessageLocalContact(boolean success);

}
