package com.example.rafael.arquitecturamvp.views.interfaces;

import com.example.rafael.arquitecturamvp.models.Contact;
import com.example.rafael.arquitecturamvp.views.IBaseView;

import java.util.ArrayList;

public interface IShowContactsView extends IBaseView {

    void getContactList(ArrayList<Contact> contactArrayList);
}
