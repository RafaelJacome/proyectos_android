package com.example.rafael.arquitecturamvp.presenters;

import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.example.rafael.arquitecturamvp.helper.Database;
import com.example.rafael.arquitecturamvp.models.Contact;
import com.example.rafael.arquitecturamvp.views.interfaces.IShowContactsView;

import java.util.ArrayList;

public class ShowContactPresenter extends BasePresenter<IShowContactsView> {

    public void showContactList() {
        getContactThreadList();
    }

    private void getContactThreadList() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                getContactFromDb();
            }
        });
        thread.start();
    }

    private void getContactFromDb() {
        try {
            ArrayList<Contact> contactList = Database.contactDao.fetchAllContacts();
            getView().getContactList(contactList);
            /*for ( Contact dto : contactList ) {
                Log.i("Contacto name: ", dto.getName() );
            }*/
        } catch (SQLiteException e) {
            Log.e("FUCK ERROR!!!!", e.getMessage() );
        }

    }
}
