package com.example.rafael.arquitecturamvp.presenters;

import android.database.sqlite.SQLiteException;
import android.nfc.Tag;
import android.util.Log;

import com.example.rafael.arquitecturamvp.helper.Database;
import com.example.rafael.arquitecturamvp.models.Contact;
import com.example.rafael.arquitecturamvp.views.interfaces.IMainViews;

import java.util.ArrayList;

public class MainPresenter extends BasePresenter<IMainViews> {

    //opcion 1
    public int sumCalculate(int one, int two) {
        return one + two;
    }

    public void calculate(int one, int two) {
        getView().showResult(one + two);
    }

    //opcion 3
    public void calcular(int one, int two) {
        getView().showToast(one + two);
        createContacts();
    }

    public void saveContacts(String name, String phone, String company){
        createContacts(name, phone, company);
    }

    private void createContacts() {
        //Contact contact = new Contact(0, name, phone, name);
        Contact contact2 = new Contact(0, "Cleria", "1234567890", "MATRIX SAS");
        //createThreadContact(contact);
        createThreadContact(contact2);
    }

    private void createContacts(String name, String phone, String company) {
        Contact contact = new Contact(0, name, phone, name);
        createThreadContact(contact);
    }

    private void createThreadContact(final Contact contact) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                createContactLocalDb(contact);
            }
        });
        thread.start();
    }

    private void createContactLocalDb(Contact contact) {
        boolean isSaved = false;
        try {
            isSaved = Database.contactDao.createContact(contact);
            getView().showMessageLocalContact(isSaved);
        } catch (Exception e) {
            getView().showMessageLocalContact(isSaved);
        }
    }


}