package com.example.rafael.arquitecturamvp.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.example.rafael.arquitecturamvp.dao.ContactDao;

public class Database {

    private static final String TAG = "Database";
    private final Context context;
    private DatabaseHelper dbHelper;

    //DAos'
    public static ContactDao contactDao;

    public Database(Context context) {
        this.context = context;
    }

    public Database open() {
        try{
            dbHelper = new DatabaseHelper(context);
            SQLiteDatabase sdb = dbHelper.getWritableDatabase();
            contactDao = new ContactDao(sdb);
            return this;
        } catch (SQLiteException ex){
            throw ex;
        }
    }

    public void close(){
        dbHelper.close();
    }
}
