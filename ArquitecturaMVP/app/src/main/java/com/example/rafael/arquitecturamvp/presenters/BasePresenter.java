package com.example.rafael.arquitecturamvp.presenters;

import com.example.rafael.arquitecturamvp.helper.ValidateInternet;
import com.example.rafael.arquitecturamvp.views.IBaseView;

public class BasePresenter<T extends IBaseView> {

    private ValidateInternet validateInternet;
    private T view;

    public void inject(T view, ValidateInternet validateInternet) {
        this.validateInternet = validateInternet;
        this.view = view;
    }

    public ValidateInternet getValidateInternet() {
        return validateInternet;
    }

    public T getView() {
        return view;
    }
}
