package com.example.rafael.arquitecturamvp.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.example.rafael.arquitecturamvp.models.Contact;
import com.example.rafael.arquitecturamvp.providers.DbContentProvider;
import com.example.rafael.arquitecturamvp.schemes.IContactosScheme;

import java.util.ArrayList;

public class ContactDao extends DbContentProvider implements IContactosScheme, IContactDao {

    private static final String TAG = "ContactDao";
    private Cursor cursor;
    private ContentValues contentValues;

    public ContactDao(SQLiteDatabase db) {
        super(db);
    }

    @Override
    protected Contact cursorToEntity(Cursor cursor) {
        Contact contact = new Contact();
        int idIndex;
        if (cursor.getColumnIndex(COLUMN_ID) != -1) {
            idIndex = cursor.getColumnIndexOrThrow(COLUMN_ID);
            contact.setId(cursor.getInt(idIndex));
        }
        if (cursor.getColumnIndex(COLUMN_NAME) != -1) {
            idIndex = cursor.getColumnIndexOrThrow(COLUMN_NAME);
            contact.setName(cursor.getString(idIndex));
        }
        if (cursor.getColumnIndex(COLUMN_PHONE) != -1) {
            idIndex = cursor.getColumnIndexOrThrow(COLUMN_PHONE);
            contact.setPhone(cursor.getString(idIndex));
        }
        if (cursor.getColumnIndex(COLUMN_COMPANY) != -1) {
            idIndex = cursor.getColumnIndexOrThrow(COLUMN_COMPANY);
            contact.setCompany(cursor.getString(idIndex));
        }
        return contact;
    }

    @Override
    public ArrayList<Contact> fetchAllContacts() {
        ArrayList<Contact> contactArrayList = new ArrayList<>();
        cursor = query(CONTACTS_TABLE, CONTACTS_COLUMNS, null,
                null, COLUMN_NAME);
        if (cursor != null) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Contact contact = cursorToEntity(cursor);
                contactArrayList.add(contact);
                cursor.moveToNext();
            }
        }
        return contactArrayList;
    }

    @Override
    public Boolean createContact(Contact contact) {
        setContentValuesContact(contact);
        try {
            return insert(CONTACTS_TABLE, getContentValues()) >= 0;
        } catch (SQLiteException ex) {
            Log.e(TAG, ex.getMessage());
            return false;
        }
    }

    public void setContentValuesContact(Contact contact) {
        contentValues = new ContentValues();
        //contentValues.put(COLUMN_ID, contact.getId());
        contentValues.put(COLUMN_NAME, contact.getName());
        contentValues.put(COLUMN_PHONE, contact.getPhone());
        contentValues.put(COLUMN_COMPANY, contact.getCompany());
    }

    public ContentValues getContentValues() {
        return contentValues;
    }
}
