package com.example.rafael.arquitecturamvp.dao;

import com.example.rafael.arquitecturamvp.models.Contact;

import java.util.ArrayList;

interface IContactDao {

    public ArrayList<Contact>  fetchAllContacts();
    public Boolean createContact(Contact contact);
}
