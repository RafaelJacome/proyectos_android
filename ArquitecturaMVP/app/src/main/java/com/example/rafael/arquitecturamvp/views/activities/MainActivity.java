package com.example.rafael.arquitecturamvp.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.rafael.arquitecturamvp.R;
import com.example.rafael.arquitecturamvp.presenters.MainPresenter;
import com.example.rafael.arquitecturamvp.views.BaseActivity;
import com.example.rafael.arquitecturamvp.views.interfaces.IMainViews;

public class MainActivity extends BaseActivity<MainPresenter> implements IMainViews {

    private EditText editText_name;
    private EditText editText_phone;
    private EditText editText_company;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setComponents();
        setPresenter(new MainPresenter());
        getPresenter().inject(this, getValidateInternet());
    }

    private void setComponents() {
        editText_name = findViewById(R.id.editText_name);
        editText_phone = findViewById(R.id.editText_phone);
        editText_company = findViewById(R.id.editText_company);
    }

    public void calculate(View view) {
        //opcion 1
        //int result = getPresenter().sumCalculate(Integer.parseInt(editText_numero1.getText().toString()),
        //       Integer.parseInt(editText_numero2.getText().toString()));
        //Toast.makeText(this, String.valueOf(result), Toast.LENGTH_SHORT).show();
        //opcion 2
        //getPresenter().calculate(Integer.parseInt(editText_numero1.getText().toString()),
        // Integer.parseInt(editText_numero2.getText().toString()));

        //opcion 3
       /* getPresenter().calcular(Integer.parseInt(editText_numero1.getText().toString()),
                Integer.parseInt(editText_numero2.getText().toString()));*/

    }

    //opcion 2
    @Override
    public void showResult(int result) {
        Toast.makeText(this, String.valueOf(result), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showMessageLocalContact(final boolean success) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String msg = success ? "Contacto creado" : "Contacto no creado";
                Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void showContacts(View view) {
       // getPresenter().showContactList();
        thisActivityToShowContactActivity();
    }

    private void thisActivityToShowContactActivity() {
        Intent intent = new Intent(this, ShowContactActivity.class);
        startActivity(intent);
    }

    public void saveContact(View view) {
        getPresenter().saveContacts(editText_name.getText().toString().trim(),
                editText_phone.getText().toString().trim(),
                editText_company.getText().toString().trim());
    }
}